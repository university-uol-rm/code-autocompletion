from pathlib import Path

from tokenizers import Tokenizer, models, normalizers, pre_tokenizers, decoders, trainers

from data.DatasetDirectory import DatasetDirectory
from finetuned_nlp.read_repo import TOKEN_FILEPATH, TOKEN_CONTENT, read_repo

tokenizer = Tokenizer(models.BPE())
tokenizer.normalizer = normalizers.Sequence([normalizers.NFD(), normalizers.StripAccents()])
tokenizer.pre_tokenizer = pre_tokenizers.ByteLevel()
tokenizer.decoders = decoders.ByteLevel()

trainer = trainers.BpeTrainer(
    vocab_size=5000,
    initial_alphabet=pre_tokenizers.ByteLevel.alphabet(),
    special_tokens=[TOKEN_FILEPATH, TOKEN_CONTENT],
)

dataset = DatasetDirectory("../data/repos/repos_py")


def read_all_repos():
    for repo in dataset.iter_split_repos("train_tokenizer"):
        print(f"reading {repo}")
        yield read_repo(repo)


tokenizer.train_from_iterator(read_all_repos(), trainer=trainer)

OUT_PATH = Path("trained_tokenizers/large-tokenizer")
OUT_PATH.mkdir(parents=True, exist_ok=True)
# tokenizer.save(str(OUT_PATH / "config.json"))
tokenizer.model.save(str(OUT_PATH))
