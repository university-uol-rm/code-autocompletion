import os

import finetuned_nlp.models as models
from data.DatasetDirectory import DatasetDirectory
from finetuned_nlp.dataset_provider import get_transformer_eval_dataset, get_transformer_train_dataset

# disable CUDA (insufficient memory)
# os.environ["CUDA_VISIBLE_DEVICES"]=""
dataset = DatasetDirectory("../data/repos/repos_py")

# model = gpt2_model(model="gpt2")
model = models.new_custom(
    layers=10,
    heads=12,
    n_embd=300,
    n_inner=600,
    tokenizer="trained_tokenizers/large-tokenizer"
)

model.train(
    train_dataset=get_transformer_train_dataset(dataset, model.tokenizer, batch_size=1),
    eval_dataset=get_transformer_eval_dataset(dataset, model.tokenizer),
    steps=1_000_000,
    checkpoint_steps=200
)
