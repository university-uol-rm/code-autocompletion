from autocompleter.Autocompleter import Autocompleter
from finetuned_nlp import models
from finetuned_nlp.read_repo import format_test_input


class TestPredictor:
    def __init__(self):
        # self.completer = Autocompleter(models.gpt_medium_finetuned())
        self.model = models.gpt_medium_finetuned()
        # self.model = models.gpt_medium()

    def predict(self, input):
        # return self.completer.complete("main.py", input)
        return self.model.predict(input, generate_length=80)