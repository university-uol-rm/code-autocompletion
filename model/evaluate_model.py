from data.DatasetDirectory import DatasetDirectory
from evaluation.count_saved_strokes import evaluate_prediction
from finetuned_nlp.models import custom_b
from finetuned_nlp.read_repo import read_file_formatted, read_file

dataset = DatasetDirectory("../data/repos/repos_py")

model = custom_b()

def evaluate_model(model):
    for repo in dataset.iter_split_repos("validation"):
        for file in repo.rglob("*"):
            if file.is_file():
                text = read_file(file)

                def predict(prompt):
                    return model.predict(prompt)

                evaluate_prediction()


if __name__ == '__main__':
    # model = custom_b()
    evaluate_model(model)
