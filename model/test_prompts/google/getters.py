class DatabaseConnection:
    url: str
    port: int
    username: str
    password: str

    def __init__(self, url, port, username, password):
        self.url = url
        self.port = port
        self.username = username
        self.password = password

    def get_url