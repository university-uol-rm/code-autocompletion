class Stack(object):
  """A stack type that proxies list's `append` and `pop` methods.

  We don't use list directly so that we can test its type for the multiple-
  dispatch that occurs in `add_grad` and `init_grad`.
  """

  def __init__(self, vals=()):
    self._stack = list(vals)

  def append(self, x):
    self._stack.append(x)

  def pop(self):
    return self._stack.pop()

  def __len__(self):
    return len(self._stack)

  def __str__(self):
    return str(self._stack)

  def __repr__(self):
    return self._stack.__repr__()