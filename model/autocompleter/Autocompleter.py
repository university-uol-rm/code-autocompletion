from pathlib import Path

import jedi

from finetuned_nlp.NlpModel import NlpModel


def with_completion(code: str, completion):
    # size = completion.get_completion_prefix_length()
    size = completion._like_name_length

    prev_code = code[:-size] if size > 0 else code

    return f"{prev_code}{completion.name_with_symbols}"


def get_rated_completions(model: NlpModel, path: Path, code: str, fuzzy=False):
    # evaluate the perplexity of each completion; lower is more likely

    script = jedi.Script(code, path=str(path))
    completions = script.complete(fuzzy=fuzzy)

    filtered_completions = [
        completion for completion in completions
        # not interested in already-complete completions
        if (not code.endswith(completion.name_with_symbols))
           # not interested in random symbol guesses, only "searched" completions
           and (not completion._like_name_length == 0)
    ]

    def evaluate_completion(completion):
        new_code = with_completion(code, completion)
        eval = model.evaluate(new_code)
        return eval

    return [(completion, evaluate_completion(completion)) for completion in filtered_completions]


class Autocompleter:
    model: NlpModel
    generation_length = 3

    def __init__(self, model: NlpModel):
        self.model = model

    def complete(self, path, code):
        rated_static_completions = get_rated_completions(self.model, path, code)

        if len(rated_static_completions) == 0:
            return self.model.predict(code, generate_length=self.generation_length)

        best_static_completion = min(
            rated_static_completions,
            key=lambda completion: completion[1]
        )[0]

        return best_static_completion.name_with_symbols[best_static_completion._like_name_length:]
