import re

perplexity = re.compile("'eval_perplexity': ([\d\.]+),")

with open("trained_models/custom-10l-12h-300ne-600ni-x117000/script_logs") as f:
    text = f.read()
    perplexities = perplexity.findall(text)
    for p in perplexities:
        print(p)