import sys

import click

from data.DatasetDirectory import DatasetDirectory


@click.command()
@click.argument('dataset_path', default="./code_dataset")
def split(
        dataset_path,
):

    dataset = DatasetDirectory(dataset_path)

    if not dataset.is_valid():
        print(f"The specified path ({dataset_path}) is not a valid dataset")
        sys.exit(1)

    dataset.split(train=10000, validation=1, test=5)


if __name__ == '__main__':
    split()
