from IPython.core.display import display, HTML


def display_evaluation(key_presses, original_length, html):
    style = """
    <style>
    .correct-prediction {
        background-color: #bbff00aa;
        border: 1px solid #0007;
        border-radius: 4px;
    }
    </style>
    """
    formatted = f"{style}<pre>{html}</pre>"
    display(HTML(formatted))

    print(f"Key presses: {key_presses}")
    print(f"Saved strokes: {(original_length - key_presses) / original_length}")