def get_typed_fragments(text, prediction_function):
    """
    Simulates the process of typing out text character by character. Whenever the prediction_function correctly predicts a continuation, accepts that prediction.
    Generates tuples of (predicted, text) that are either:
    - (True, prediction) if the prediction_function returned a correct prediction (which was accepted)
    - (False, character) if there was no prediction or it was inaccurate. The second item is the manually typed-out character
    :param text: The string to evaluate
    :param prediction_function: Given a string, should return a continuation prediction or None
    :return:
    """
    cursor_index = 0

    while cursor_index < len(text):
        current_text = text[:cursor_index]
        continuation = text[cursor_index:]

        prediction = None if cursor_index == 0 else prediction_function(current_text)
        is_correct = (prediction is not None) and continuation.startswith(prediction)

        if is_correct:
            cursor_index += len(prediction)
            yield True, prediction
        else:
            cursor_index += 1
            yield False, continuation[0]


def evaluate_prediction(text, prediction_function):
    count = 0
    rendered_result = ""

    for (is_prediction, text) in get_typed_fragments(text, prediction_function):
        count += 1
        if is_prediction:
            rendered_result += f"<span class='correct-prediction'>{text}</span>"
        else:
            rendered_result += text

    return count, rendered_result
