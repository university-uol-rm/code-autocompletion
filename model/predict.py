from TestPredictor import TestPredictor

predictor = TestPredictor()

while True:
    print("---Prompt---")

    lines = []
    while True:
        line = input()
        if line == "!":
            break

        lines.append(line)

    prompt = "\n".join(lines)

    output = predictor.predict(prompt)

    print(f"---Output---")
    print(output)
