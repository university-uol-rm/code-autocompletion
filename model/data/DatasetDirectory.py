import tokenize
from pathlib import Path

from humanize import naturalsize

SOURCE_DIR = 'source_code'
PREPROCESS_DIR = 'preprocess'


def directory_size(directory: Path):
    """
    Given a directory, calculates the approximate size of the directory
    by summing the sizes of the files and directories it contains.
    Based on https://stackoverflow.com/a/1392549
    :param directory: the directory to calculate the size of
    :return: the size of the directory
    """
    return sum(file.stat().st_size for file in directory.rglob("*") if not file.is_symlink())


class DatasetDirectory:
    path: Path

    def __init__(self, path: str) -> None:
        self.path = Path(path)

    def is_valid(self):
        return self.path.is_dir()

    def get_source_path(self, filename=""):
        source_path = self.path / SOURCE_DIR
        if not source_path.is_dir():
            raise Exception(
                f"Error; dataset {self.path} has no source_code directory")

        return source_path / filename

    def get_preprocessed_path(self, filename=""):
        preprocessed_path = self.path / PREPROCESS_DIR
        preprocessed_path.mkdir(exist_ok=True)

        return preprocessed_path / filename

    def iter_source_files(self):
        for file in self.get_source_path().rglob("*"):
            if file.is_file():
                # auto-detects encoding
                with tokenize.open(file) as open_file:
                    yield open_file

    def iter_repos(self):
        for author in self.get_source_path().glob("*"):
            for repo in author.glob("*"):
                yield repo

    def iter_split_repos(self, split):
        split_path = self.get_split_file(split)

        with open(split_path) as file:
            for line in file:
                if line == "":
                    continue # ignore empty lines
                yield self.path / line[:-1]  # remove trailing newline

    def make_split(self, **kwargs):
        """ Splits the dataset into the specified proportions """

        # get a list of repos, largest first
        index = [(path, directory_size(path)) for path in self.iter_repos()]
        index.sort(key=lambda item: item[1], reverse=True)

        cutoff = len(index) // 2
        # use the largest 50% of repos for training only.
        # this avoids the issue of having the test split only consist of e.g. one huge repository,
        # which wouldn't be representative of the data
        train_only = index[:cutoff]
        rest = index[cutoff:]

        splits = {
            split: [0., set()]
            for split in kwargs.keys()
        }

        # a value representing how much data a split has relative to how much it needs
        def weighed_size(split):
            name, value = split
            return value[0] / kwargs[name]

        train_split = splits["train"]
        for (repo, size) in train_only:
            train_split[0] += size
            train_split[1].add(repo)

        # we give each repo to the split that "needs" it the most
        # call it a greedy naive algorithm but it works for the huge amounts of repos we have
        for (repo, size) in rest:
            emptiest = min(splits.items(), key=weighed_size)[1]
            emptiest[0] += size
            emptiest[1].add(repo)

        return splits

    def get_split_file(self, name):
        split_directory = self.path / "split"
        split_directory.mkdir(exist_ok=True)
        return split_directory / f"{name}.txt"

    def save_split(self, name, repos):
        output_path = self.get_split_file(name)

        with open(output_path, mode="x") as file:
            file.writelines(
                f"{str(repo.relative_to(self.path))}\n"
                for repo in repos
            )

    def split(self, train=8, validation=1, test=1):
        splits = self.make_split(train=train, validation=validation, test=test)

        for name, [size, repos] in splits.items():
            self.save_split(name, repos)
            print(f"split {name} size: {naturalsize(size)}")

    def split_info(self):
        for name in ("train", "validation", "test"):
            size = sum(directory_size(path) for path in self.iter_split_repos(name))
            print(f"split {name} size: {naturalsize(size)}")

