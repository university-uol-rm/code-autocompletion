from flask import Flask, request, jsonify

from TestPredictor import TestPredictor
from flask_cors import CORS

predictor = TestPredictor()

app = Flask(__name__)
CORS(app)


@app.route('/predict')
def index():
    prompt = request.args.get('prompt')

    if not isinstance(prompt, str):
        return "Prompt not specified", 400

    output = predictor.predict(prompt)
    return jsonify({
        'suggestion': output
    })
