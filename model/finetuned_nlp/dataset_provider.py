from finetuned_nlp.data.BatchedDataset import BatchedDataset
from finetuned_nlp.data.ChunkedDataset import ChunkedDataset
from finetuned_nlp.data.EvaluationDataset import EvaluationDataset
from finetuned_nlp.data.RepoDataset import RepoDataset


def get_transformer_train_dataset(dataset, tokenizer, batch_size=1):
    source = RepoDataset(dataset, "train", tokenizer)
    chunked = ChunkedDataset(source, max_tokens=1024)
    return BatchedDataset(chunked, batch_size=batch_size)


def get_transformer_eval_dataset(
        dataset,
        tokenizer
):
    source = RepoDataset(dataset, "validation", tokenizer)
    chunked = ChunkedDataset(source, max_tokens=1024, is_random_order=False)
    return EvaluationDataset(chunked)
