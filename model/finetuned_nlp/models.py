from transformers import GPT2LMHeadModel, GPT2TokenizerFast, GPT2Config

from finetuned_nlp.NlpModel import NlpModel


def _gpt_based(model_path, tokenizer_path="gpt2"):
    return NlpModel(
        model=GPT2LMHeadModel.from_pretrained(model_path),
        tokenizer=GPT2TokenizerFast.from_pretrained(tokenizer_path)
    )


def gpt_small():
    return _gpt_based("gpt2")


def gpt_medium():
    return _gpt_based("gpt2-medium")


def gpt_small_finetuned():
    return _gpt_based("./trained_models/gpt2-small-3000x4/")


def gpt_medium_finetuned():
    return _gpt_based("./trained_models/gpt2-medium-3000x1/")


def new_custom(layers=8, heads=6, n_embd=768, n_inner=4 * 768, tokenizer="trained_tokenizers/my-tokenizer"):
    tokenizer = GPT2TokenizerFast.from_pretrained(tokenizer)

    token_count = len(tokenizer.get_vocab())
    configuration = GPT2Config(
        vocab_size=token_count + 1,
        n_layer=layers,
        n_head=heads,
        n_embd=n_embd,
        n_inner=n_inner,
        eos_token_id=token_count,
        bos_token_id=token_count
    )
    model = GPT2LMHeadModel(configuration)

    return NlpModel(
        model=model,
        tokenizer=tokenizer
    )


def _custom_trained(path):
    return NlpModel(
        model=GPT2LMHeadModel.from_pretrained(path),
        tokenizer=GPT2TokenizerFast.from_pretrained("trained_tokenizers/my-tokenizer")
    )


def custom_6l_12h():
    return _custom_trained("trained_models/custom-6l-12h-300ne-600ni-31000/checkpoint-31000")


def custom_10l_12h():
    return _custom_trained("trained_models/custom-10l-12h-300ne-600ni-x117000/checkpoint-117000")
