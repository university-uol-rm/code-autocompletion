import pathlib

TOKEN_FILEPATH = "<|path|>"
TOKEN_CONTENT = "<|content|>"


def read_file(path: pathlib.Path):
    with open(path, 'r') as file:
        return file.read()


def format_test_input(input):
    return f"{TOKEN_FILEPATH} main.py {TOKEN_CONTENT} {input}"


def read_file_formatted_parts(repo: pathlib.Path, file: pathlib.Path):
    yield TOKEN_FILEPATH
    yield str(file.relative_to(repo))
    yield TOKEN_CONTENT
    yield read_file(file)


def read_file_formatted(repo: pathlib.Path, file: pathlib.Path):
    return " ".join(read_file_formatted_parts(repo, file))


def read_repo_parts(repo: pathlib.Path):
    for file in repo.rglob("*"):
        if not file.is_file():
            continue

        yield from read_file_formatted_parts(repo, file)


def read_repo(repo: pathlib.Path):
    return " ".join(read_repo_parts(repo))
