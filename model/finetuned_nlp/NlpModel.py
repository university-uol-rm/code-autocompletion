import math

import torch
import torch.nn as nn
from transformers import Trainer, TrainingArguments

cross_entropy_loss = nn.CrossEntropyLoss()


def compute_cross_entropy_loss(outputs, targets):
    # for whatever reason, this data is an np array, not a torch tensor
    batch = len(outputs)
    return (sum(
        cross_entropy_loss(
            # need to align predictions with targets, cos apparently the lib dont do that for us
            torch.tensor(outputs[i, :-1]),
            torch.tensor(targets[i, 1:])
        )
        # CrossEntropyLoss doesnt accept batches, de-batch
        for i in range(batch)
    ) / batch).item()


def compute_perplexity(outputs, targets):
    return math.exp(compute_cross_entropy_loss(outputs, targets))


def metrics(eval_prediction):
    return {
        "perplexity": compute_perplexity(eval_prediction.predictions, eval_prediction.label_ids),
    }


class NlpModel:
    def __init__(self, model, tokenizer, use_gpu=False, token_size=1024):
        self.tokenizer = tokenizer
        self.model = model

        self.use_gpu = use_gpu
        if use_gpu:
            self.model.parallelize()

        self.token_size = token_size

    def train(
            self,
            train_dataset,
            eval_dataset,
            steps=1000,
            checkpoint_steps=10,
    ):
        train_dataset = train_dataset
        validation_dataset = eval_dataset

        training_args = TrainingArguments(
            output_dir='./trained',
            overwrite_output_dir=True,

            logging_dir='./logs',
            logging_steps=checkpoint_steps,

            max_steps=steps,
            warmup_steps=steps // 100,

            per_device_train_batch_size=1,

            save_steps=checkpoint_steps,
            save_total_limit=3,

            evaluation_strategy="steps",
        )

        trainer = Trainer(
            model=self.model,
            args=training_args,
            train_dataset=train_dataset,
            eval_dataset=validation_dataset,
            compute_metrics=metrics
        )

        self.model.train()
        trainer.train()
        trainer.save_model()

    def evaluate(self, input):
        """
        Evaluates the likelihood of the last `token_size` tokens of the specified string
        :param input:
        :return:
        """
        tokenized = self.tokenizer(input, return_tensors="pt", verbose=False)
        input_ids = tokenized["input_ids"][:, -self.token_size:].to(self.model.device)
        logits = self.model.forward(input_ids).logits
        return compute_perplexity(logits, input_ids)


    def predict(self, input, generate_length=10):
        max_input_size = self.token_size - generate_length

        tokenized = self.tokenizer(input, return_tensors="pt", verbose=False)
        input_ids = tokenized["input_ids"][:, -max_input_size:].to(self.model.device)
        existing_size = input_ids.size(-1)

        generated = self.model.generate(
            input_ids,
            max_length=existing_size + generate_length,
            eos_token_id=self.tokenizer.eos_token_id,
            pad_token_id=self.tokenizer.eos_token_id  # set pad to eos (apparently needed for open-end (??) generation?)
        )

        full_text = self.tokenizer.decode(generated[0][existing_size:])
        return full_text
