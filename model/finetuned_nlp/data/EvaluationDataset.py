import torch


class EvaluationDataset(torch.utils.data.Dataset):
    def __init__(self, source: torch.utils.data.Dataset, limit=10):
        self.data = [item for (_, item) in zip(range(limit), source)]

    def __getitem__(self, key):
        return self.data[key]

    def __len__(self):
        return len(self.data)
