import random
import math

import torch

from finetuned_nlp.data.RepoDataset import RepoDataset


def tokens_to_chunks(tokens, chunk_size=1024, offsets=(0,)):
    """
    Given a sequence of tokens, returns a sequence of chunks
    Each chunk has size chunk_size and is a subsequence of tokens
    """

    tokens_pad = torch.nn.functional.pad(tokens, [0, chunk_size], mode='constant', value=0)

    chunks = []

    for offset in offsets:
        chunk_count = math.ceil((tokens.shape[0] - offset) / chunk_size)
        size = chunk_count * chunk_size

        chunks.append(tokens_pad[offset:offset + size].reshape((-1, chunk_size)))

    return torch.cat(chunks, 0)


class ChunkedDataset(torch.utils.data.IterableDataset):
    def __init__(self, source: RepoDataset, max_tokens=1024, is_random_order=True):
        self.source = source
        self.max_tokens = max_tokens

        self.is_random_order = is_random_order
        self.current_index = 0

    def get_next_repo(self):
        if self.is_random_order:
            i = random.randint(0, len(self.source) - 1)
        else:
            i = self.current_index
            self.current_index += 1

            if i >= len(self.source):
                return None

        return self.source[i]

    def generate_samples(self):
        while True:
            tokens = self.get_next_repo()
            if tokens is None:
                break

            features = [f for f in ("input_ids", "attention_mask") if f in tokens]

            feature_chunks = {
                feature: tokens_to_chunks(tokens[feature][0], self.max_tokens, (0, self.max_tokens // 2))
                for feature in features
            }

            for i in range(len(feature_chunks["input_ids"])):
                sample = {
                    feature: feature_chunks[feature][i]
                    for feature in features
                }

                yield {
                    "labels": sample["input_ids"].detach().clone(),
                    **sample
                }

    def __iter__(self):
        return iter(self.generate_samples())
