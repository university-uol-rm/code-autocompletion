import torch


class BatchedDataset(torch.utils.data.IterableDataset):
    def __init__(self, source: torch.utils.data.IterableDataset, batch_size=20):
        self.sample_iter = iter(source)
        self.batch_size = batch_size

    def generate_batches(self):
        while True:
            batch_samples = [next(self.sample_iter) for _ in range(self.batch_size)]

            features = [f for f in ("input_ids", "labels", "attention_mask") if f in batch_samples[0]]
            batch = {
                feature: torch.stack([sample[feature] for sample in batch_samples])
                for feature in features
            }

            yield batch

    def __iter__(self):
        return iter(self.generate_batches())
