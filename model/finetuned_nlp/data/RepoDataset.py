import torch

from data.DatasetDirectory import DatasetDirectory
from finetuned_nlp.read_repo import read_repo


class RepoDataset(torch.utils.data.Dataset):
    def __init__(self, dataset_path: DatasetDirectory, split, tokenizer):
        self.repos = list(dataset_path.iter_split_repos(split))
        self.tokenizer = tokenizer

    def compute_item(self, i):
        text = read_repo(self.repos[i])
        # disable verbose.
        # we don't want to generate warnings about sequence length because we are handling that later.
        return self.tokenizer(text, return_tensors="pt", verbose=False)

    def __getitem__(self, i):
        return self.compute_item(i)

    def __len__(self):
        return len(self.repos)