# Configuration
Create an `.env` file.  Set GITHUB_PERSONAL_TOKEN to a [Github personal access token](https://github.com/settings/tokens) that has access to public repositories.

# Download repositories
`download_with_limit.py` downloads a dataset of the most popular repositories. Example usage:

```bash
python ./download_with_limit.py python_repos --language python

python ./download_with_limit.py repos_js --language javascript
```
