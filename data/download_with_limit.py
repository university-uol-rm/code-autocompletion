import csv
import os
import sys
from pathlib import Path
from typing import Set
import click

import throttle
from humanize import naturalsize

from extensions import DEFAULT_EXTENSIONS
from util.clean_repo import clean_directory
from util.file import directory_size
from util.persevere import persevere
from util.repo import get_top_repos_for_language, clone_repo


def download_with_limit(
        output_path: Path,
        dataset_size: int,
        language: str,
        licenses: Set[str],
        extension_whitelist: Set[str]
):
    repos = get_top_repos_for_language(language, licenses)

    @throttle.wrap(10, 1)  # update at most once every 10 seconds
    def update(_, done, max, message):
        """
        utility for printing occasional updates about download progress
        """
        print(f"{done / max * 100:.0f}% - {message}")

    total_size = directory_size(output_path / 'source_code')
    with open(output_path / 'sources.csv', 'a', newline='') as csv_file:
        index_writer = csv.writer(csv_file)

        for repo in persevere(repos):
            if total_size >= dataset_size:
                break

            repo_path = output_path / 'source_code' / repo.full_name

            repo_data = (
                repo.html_url,
                # ugly but seems to work without additional requests
                repo._rawData['license']['name']
            )
            index_writer.writerow(repo_data)

            if repo_path.is_dir():
                print(f"{repo.html_url} seems to be already downloaded; skipping.")
            else:
                clone_repo(repo, repo_path, update)

            clean_directory(repo_path, extension_whitelist)
            total_size += directory_size(repo_path)
            print()

    print(f"finished downloading. dataset size: {naturalsize(total_size)}.")


def validate_output_directory(output_path, is_resume):
    if output_path.is_file():
        print("Output target is a file, abort")
        sys.exit(1)

    if is_resume:
        if not output_path.is_dir():
            print("Cannot resume download - target directory does not exist")
            sys.exit(3)
    else:
        if output_path.is_dir() and os.listdir(output_path):
            print(
                "Output directory not empty, abort. if you would like to resume the download, specify this explicitly")
            sys.exit(2)

        output_path.mkdir(exist_ok=True)


DEFAULT_LICENSES = {
    "mit",
    "apache-2.0",
    "gpl-2.0", "gpl-3.0",
    "bsd-3-clause", "bsd-2-clause",
    "unlicense",
}


@click.command()
@click.pass_context
@click.argument('output_path', default="./code_dataset")
@click.option('--language', help='Programming language to filter by', required=True)
@click.option('--size', default=5_000_000_000, help='Amount of data to download, in bytes', show_default="5GB")
@click.option('--licenses', default=None,
              help='Licenses to allow (comma-separated)',
              show_default="5 open source licenses")
@click.option('--extensions', default=None,
              help='Filename extensions to keep (comma-separated) - other files will be deleted',
              show_default="Automatically selected by language"
              )
@click.option('--resume', is_flag=True, default=False, help='Resume download of an existing dataset')
def cli(
        context,
        output_path,
        language,
        size,
        licenses,
        extensions,
        resume
):
    output_path = Path(output_path)
    validate_output_directory(output_path, resume)

    if licenses is None:
        licenses = DEFAULT_LICENSES
    else:
        licenses = set(licenses.split(","))

    if extensions is None:
        if language not in DEFAULT_EXTENSIONS:
            print(
                f"The language {language} does not have a pre-configured set of allowed extensions, so you need to specify those explicitly using the --extensions option"
            )
            sys.exit(3)
        extensions = DEFAULT_EXTENSIONS[language]
    else:
        extensions = set(extensions.split(","))
    extensions = {f".{extension}" for extension in extensions}

    print(f"Will download at least {naturalsize(size)} worth of {language} source code to {output_path}.")
    print(f"Licenses: {', '.join(licenses)}")
    print(f"Extensions: {', '.join(extensions)}")

    try:
        proceed = click.confirm("Proceed?", default=True)
        if not proceed:
            sys.exit(0)
    except click.Abort:
        sys.exit(0)

    print("Downloading...")
    download_with_limit(output_path, size, language, licenses, extensions)


if __name__ == '__main__':
    cli()
