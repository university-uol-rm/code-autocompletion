import sys
from pathlib import Path

import click

from util.clean_repo import clean_directory


@click.command()
@click.argument('path', default="./code_dataset")
@click.option('--extensions', default=None,
              help='Filename extensions to keep (comma-separated) - other files will be deleted',
              required=True
              )
def cli(path, extensions):
    path = Path(path)

    if path.is_file():
        print("Output directory is a file")
        sys.exit(1)

    extensions = set(f".{extension}" for extension in extensions.split(","))

    print("Cleaning...")
    clean_directory(path / "source_code", extensions)
    print("Done.")


if __name__ == '__main__':
    cli()
