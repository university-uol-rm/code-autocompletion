import os
import re
import itertools
from pathlib import Path

#  future command-line parameters
path = Path('./repos')

# https://www.ibm.com/support/knowledgecenter/en/SSLTBW_2.2.0/com.ibm.zos.v2r2.bpxa400/bpxug469.htm
banned_chars = re.compile("[^a-zA-Z0-9._-]+")


def is_illegal(name: str):
    return banned_chars.search(name) is not None


def clean_name(original: str):
    return banned_chars.sub("_", original)


def dedupe_name(name: str):
    yield name

    for i in itertools.count(1):
        yield f"{name}_{i}"


def posix_convert(path):
    for root, directories, files in os.walk(path, topdown=False):
        children = [*files, *directories]
        blacklist = {*children}

        for name in children:
            if not is_illegal(name):
                continue

            cleaned = clean_name(name)
            new_name = next(
                candidate
                for candidate in dedupe_name(cleaned)
                if candidate not in blacklist
            )
            blacklist.add(new_name)

            old_path = os.path.join(root, name)
            new_path = os.path.join(root, new_name)

            child_type = "directory" if name in directories else "file"
            print(f"renaming {child_type} {old_path} to {new_name}")
            os.rename(old_path, new_path)


posix_convert(path)
