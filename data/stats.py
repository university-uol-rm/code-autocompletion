import os
import re
import itertools
from pathlib import Path
from typing import NamedTuple

from humanize import naturalsize

from util.file import directory_size

#  future command-line parameters

path = Path('./repos/repos_py')


def gather_stats(path):
    source_code = path / "source_code"

    author_count = 0
    repo_count = 0
    total_size = 0
    largest_repo_size = 0

    for author in source_code.glob("*"):
        author_count += 1
        for repo in author.glob("*"):
            repo_count += 1
            size = directory_size(repo)
            total_size += size
            largest_repo_size = max(largest_repo_size, size)

    return {
        "author count": author_count,
        "repo count": repo_count,
        "total size": naturalsize(total_size),
        "largest repo size": naturalsize(largest_repo_size)
    }


print("gathering statistics...")
stats = gather_stats(path)

for stat, value in stats.items():
    print(f"{stat}: {value}")
