import os
import shutil
from pathlib import Path

directory_blacklist = {
    '.git',
    '.github'
}


def should_delete_file(file, extension_whitelist: set):
    _, extension = os.path.splitext(file)

    if extension not in extension_whitelist:
        return True

    if os.path.islink(file):
        return True

    try:
        with open(file) as file:
            for _ in file:
                pass  # make sure all lines can be read
    except UnicodeDecodeError:
        return True

    return False


def should_delete_directory(directory):
    return os.path.basename(directory) in directory_blacklist


def clean_directory(path: Path, extension_whitelist):
    for root, directories, files in os.walk(path):
        for file in files:
            file_path = os.path.join(root, file)
            if should_delete_file(file_path, extension_whitelist):
                os.remove(os.path.join(root, file))

        for directory in directories:
            if should_delete_directory(directory):
                shutil.rmtree(os.path.join(root, directory))
                directories.remove(directory)
