import time
from datetime import timedelta, datetime

from github import RateLimitExceededException
from github.PaginatedList import PaginatedListBase
from humanize import naturaldelta

github_search_limit = 30
github_limit_reset = timedelta(seconds=60)
safety_margin = timedelta(seconds=5)
fail_delay = timedelta(seconds=15)


def persevere(api_paginated_list: PaginatedListBase):
    index = 0
    requests_made = []

    while True:
        # yield already-fetched items as long as possible
        while not len(api_paginated_list._PaginatedListBase__elements) <= index: # 12AM code; apologies to anyone debugging this.
            yield api_paginated_list[index]
            index+=1

        if not api_paginated_list._couldGrow():
            print("[no more items]")
            break

        # to yield the next one, we need to make a request.

        # subtract safety margin for ++ error resilience
        now = datetime.now() - safety_margin
        requests_made = [
            request_time for request_time in requests_made
            if request_time > now - github_limit_reset
        ]

        if len(requests_made) >= github_search_limit:
            next_chance = min(requests_made) + github_limit_reset
            waiting_time = next_chance - now
            print(f"[waiting {naturaldelta(waiting_time)} to avoid exceeding Github search rate limit]")
            time.sleep(waiting_time.seconds)

        try:
            # next pagination request
            print("[making Github request]")
            api_paginated_list._PaginatedListBase__fetchToIndex(index)
            requests_made.append(datetime.now())
        except RateLimitExceededException:
            print(f"[Github rate limit exceeded unexpectedly - waiting {naturaldelta(fail_delay)}]")
            # pray this works
            time.sleep(fail_delay.seconds)
