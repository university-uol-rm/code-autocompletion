import os
from pathlib import Path

from dotenv import load_dotenv

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

GITHUB_PERSONAL_TOKEN = os.getenv("GITHUB_PERSONAL_TOKEN")
