from pathlib import Path

# todo this function is duplicated in 2 repos fo the monorepo, refactor and merge
def directory_size(directory: Path):
    """
    Given a directory, calculates the approximate size of the directory
    by summing the sizes of the files and directories it contains.
    Based on https://stackoverflow.com/a/1392549
    :param directory: the directory to calculate the size of
    :return: the size of the directory
    """
    return sum(file.stat().st_size for file in directory.rglob("*") if not file.is_symlink())
