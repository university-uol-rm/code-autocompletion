import datetime
import re
import time

import humanize
from git import Repo
from github import Github, Repository

from util.read_env import GITHUB_PERSONAL_TOKEN


def get_top_repos_for_language(language, licenses):
    g = Github(GITHUB_PERSONAL_TOKEN)

    # the library does not allow multiple values, but the api does
    # so we construct this part of the query manually
    query = " ".join(f"license:{l}" for l in licenses)

    return g.search_repositories(
        query,
        language=language,
        sort="stars",
        order="desc"
    )


github_clone_url_regex = re.compile("^https://github.com/(.+)$")


def add_token_to_url(url: str):
    """
    Given a Github clone URL, adds the personal access token to it.
    This authentication enables larger limits on how fast we can clone repos.
    :param url: a Github clone URL
    :return: the same URL, with the personal access token
    """
    token = GITHUB_PERSONAL_TOKEN

    if not isinstance(token, str):
        raise Exception("No Github Personal Access Token provided")

    parsed = github_clone_url_regex.match(url)
    if parsed is None:
        raise Exception("Expected URL to be a Github clone URL, but could not match.")

    (path,) = parsed.groups()
    return f"https://{token}@github.com/{path}"


def clone_repo(repo: Repository.Repository, path, update):
    print(f"cloning {repo.html_url} to {path}")

    fail_delay = datetime.timedelta(seconds=30)
    while True:
        try:
            auth_url = add_token_to_url(repo.clone_url)
            Repo.clone_from(auth_url, path, update)
            break
        except Exception as e:
            print(e)
            # we probably annoyed Github too much
            print(f"could not clone. retrying in {humanize.naturaldelta(fail_delay)}")
            time.sleep(fail_delay.seconds)
            fail_delay *= 2

    print("done")
