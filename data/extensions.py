DEFAULT_EXTENSIONS = {
    "python": {"py"},
    "javascript": {'js', 'ts', 'jsx', 'tsx'},
    "java": {'java'}
}